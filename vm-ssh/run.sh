#!/bin/bash
# Creation du network vaultnet
if [ $(docker network ls | grep 'vaultnet' > /dev/null 2>&1; echo $?) -ne 0 ]; then
	echo "Creation du docker network 'vaultnet'."
	docker network create --driver bridge vaultnet
fi

cd $(pwd)/$(dirname $0)
docker build . -t sshd-signer:latest
docker run -it --name ssh-signer -h ssh-signer.vaultnet --dns-search=vaultnet --network vaultnet --rm -d sshd-signer:latest

while [ $(docker logs ssh-signer 2>&1 | egrep '^===========' > /dev/null; echo $?) -ne 0 ]
do
	echo -n '.'
	sleep 1
done
echo ""

docker logs ssh-signer | egrep -m 1 -A 5 '^=============================================='

exit 0
