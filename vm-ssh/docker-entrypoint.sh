#!/bin/sh

if [ ! -f "/etc/ssh/ssh_host_rsa_key" ]; then
	# generate fresh rsa key
	ssh-keygen -f /etc/ssh/ssh_host_rsa_key -N '' -t rsa
fi
if [ ! -f "/etc/ssh/ssh_host_dsa_key" ]; then
	# generate fresh dsa key
	ssh-keygen -f /etc/ssh/ssh_host_dsa_key -N '' -t dsa
fi

#prepare run dir
if [ ! -d "/var/run/sshd" ]; then
  mkdir -p /var/run/sshd
fi

# check de host.docker.internal
if [ $(grep host.docker.internal /etc/hosts > /dev/null; echo $?) -ne 0 ]; then
	echo "$(ip -4 route show default | cut -d ' ' -f '3')  host.docker.internal" >> /etc/hosts
fi

# Check du Trust du certificat
if [ -r /etc/ssh/trusted-user-ca-keys.pem ] && [ $(egrep '^TrustedUserCAKeys' /etc/ssh/sshd_config > /dev/null 2>&1; echo $?) -ne 0 ]; then
	echo "
# For client keys
TrustedUserCAKeys /etc/ssh/trusted-user-ca-keys.pem" >> /etc/ssh/sshd_config
fi

# Creation du user node (passwd : node)
echo "Modification du password du user 'node'"
mon_pass='node'
echo "${mon_pass}
${mon_pass}" | passwd node

chown -R node:users /home/node

# Affichage banniere
echo "
=====================================================
VM : 
     - name : $(hostname -a) ('$(hostname)')
     - IP : $(hostname -i)
     - SSH : ssh -o StrictHostKeyChecking=no node@$(hostname -i)
====================================================="

exec "$@"
