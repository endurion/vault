#!/bin/sh


# Creation du network vaultnet
docker network ls | grep 'vaultnet' > /dev/null 2>&1
if [ $? -ne 0 ]; then
	echo "Creation du docker network 'vaultnet'."
	docker network create --driver bridge vaultnet
fi

#docker run --cap-add=IPC_LOCK -e 'VAULT_DEV_ROOT_TOKEN_ID=myroot' -e 'VAULT_DEV_LISTEN_ADDRESS=0.0.0.0:1234' -p 8200:1234 -d --name=dev-vault vault
docker run --cap-add=IPC_LOCK -e 'VAULT_DEV_LISTEN_ADDRESS=0.0.0.0:1234' -p 8200:1234 -d --name=dev-vault --rm -h vault.example.net --network vaultnet vault

while [ $(docker logs dev-vault 2>&1 | egrep '^Root Token' > /dev/null; echo $?) -ne 0 ]
do
	echo -n '.'
	sleep 1
done
echo ""
docker logs dev-vault 2>&1 | egrep '^Root Token:|^Unseal Key:'
echo "
\t$ export VAULT_ADDR='http://127.0.0.1:8200'
\t$ vault login $(docker logs dev-vault 2>&1 | egrep '^Root Token' | cut -d' ' -f 3)"
