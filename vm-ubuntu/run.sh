#!/bin/bash
# Creation du network vaultnet
if [ $(docker network ls | grep 'vaultnet' > /dev/null 2>&1; echo $?) -ne 0 ]; then
	echo "Creation du docker network 'vaultnet'."
	docker network create --driver bridge vaultnet
fi

cd $(pwd)/$(dirname $0)
docker build . -t sshd:latest
docker run -it -p 8022:22 --name vm1 -h vm1.example.net --dns-search=example.net --network vaultnet --rm -d sshd:latest

while [ $(docker logs vm1 2>&1 | egrep '^===========' > /dev/null; echo $?) -ne 0 ]
do
	echo -n '.'
	sleep 1
done
echo ""

docker logs vm1 | egrep -m 1 -A 5 '^=============================================='
