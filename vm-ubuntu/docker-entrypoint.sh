#!/bin/sh

if [ ! -f "/etc/ssh/ssh_host_rsa_key" ]; then
	# generate fresh rsa key
	ssh-keygen -f /etc/ssh/ssh_host_rsa_key -N '' -t rsa
fi
if [ ! -f "/etc/ssh/ssh_host_dsa_key" ]; then
	# generate fresh dsa key
	ssh-keygen -f /etc/ssh/ssh_host_dsa_key -N '' -t dsa
fi

#prepare run dir
if [ ! -d "/var/run/sshd" ]; then
  mkdir -p /var/run/sshd
fi

# Ajout du docker host dans /etc/hosts
echo "$(ip -4 route show default | cut -d ' ' -f '3')  host.docker.internal" >> /etc/hosts

# Creation du user node (passwd : node)
echo "Modification du password du user 'node'"
mon_pass='node'
echo "${mon_pass}
${mon_pass}" | passwd node
echo "
#### Configuration de Vault
export VAULT_ADDR='http://host.docker.internal:8200'" >> /home/node/.bashrc

chown -R node:users /home/node

# Affichage banniere
echo "
=====================================================
VM : 
     - name : $(hostname -a) ('$(hostname)')
     - IP : $(hostname -i)
     - SSH : ssh -o StrictHostKeyChecking=no node@$(hostname -i)
====================================================="

exec "$@"
