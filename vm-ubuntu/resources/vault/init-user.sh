#!/bin/sh

cd $(pwd)/$(dirname $0)

if [ -z "${VAULT_ADDR}" ]; then
	# Par defaut l'adresse de vault depuis le Host
	export VAULT_ADDR='http://127.0.0.1:8200'
fi

if [ -x /usr/bin/docker ]; then
	# PAr defaut, récupération des crédentials depuis le docker du Host
	vault login $(docker logs dev-vault 2>&1 | egrep '^Root Token' | cut -d' ' -f 3)
fi

if [ $(vault secrets list >/dev/null 2>&1; echo $?) -ne 0 ]; then
	echo "Unable to connect to Vault"
	exit 1
fi

echo "Creation du policy admin"
vault policy write admin admin-policy.hcl

echo "Creation du policy provisionner"
vault policy write provisioner provisioner-policy.hcl

echo "List des policies"
vault policy list

echo "Creation de l'Entity 'administrator'"
if [ $(vault list identity/entity/name 2>/dev/null | grep administrator > /dev/null; echo $?) -ne 0 ]
then
	vault write identity/entity name="administrator" metadata=organization="ACME Inc."
fi

echo "Creation du groupe 'admin-group'"
if [ $(vault list identity/group/name 2>/dev/null | grep 'admin-group' > /dev/null; echo $?) -ne 0 ]; then
	vault write identity/group name="admin-group" policies="admin" member_entity_ids=$(vault read -format=json identity/entity/name/administrator | jq '.data.id' | tr -d '"')  metadata=team="Vault Administors"
fi

if [ $(vault auth list | egrep '^userpass' > /dev/null; echo $?) -ne 0 ]; then
	echo "Ajout du moteur userpass"
	vault auth enable userpass
fi

if [ $(vault list auth/userpass/users 2> /dev/null | egrep '^admin' > /dev/null; echo $?) -ne 0 ]; then
	echo "Creation du user 'admin'"
	vault write auth/userpass/users/admin password="admin"
	echo "Association du user 'admin' à l'entity 'administrator'"
	vault write identity/entity-alias name="admin" \
		canonical_id="$(vault read -format=json identity/entity/name/administrator | jq '.data.id' | tr -d '"')" \
		mount_accessor="$(vault auth list -format=json | jq -r '.["userpass/"].accessor')"
fi

echo "Creation de l'Entity 'provisioner'"
if [ $(vault list identity/entity/name 2>/dev/null | grep 'provisioner' > /dev/null; echo $?) -ne 0 ]
then    
        vault write identity/entity name="provisioner" metadata=organization="ACME Inc."
fi

echo "Creation du groupe 'provision-group'"
if [ $(vault list identity/group/name 2>/dev/null | grep 'provision-group' > /dev/null; echo $?) -ne 0 ]; then
        vault write identity/group name="provision-group" policies="provisioner" member_entity_ids=$(vault read -format=json identity/entity/name/provisioner | jq '.data.id' | tr -d '"')  metadata=team="Vault Data Administors"
fi

if [ $(vault list auth/userpass/users 2> /dev/null | egrep '^provision' > /dev/null; echo $?) -ne 0 ]; then
        echo "Creation du user 'provision'"
        vault write auth/userpass/users/provision password="provision"
        echo "Association du user 'provision' à l'entity 'provisioner'"
        vault write identity/entity-alias name="provision" \
                canonical_id="$(vault read -format=json identity/entity/name/provisioner | jq '.data.id' | tr -d '"')" \
                mount_accessor="$(vault auth list -format=json | jq -r '.["userpass/"].accessor')"
fi      


