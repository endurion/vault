Doc : 

- https://learn.hashicorp.com/vault/secrets-management/sm-dynamic-secrets
- https://learn.hashicorp.com/vault/secrets-management/db-root-rotation

prérequis :

- docker : pour lancer la base `postgresql` de test
- client `postgresql` : ```sudo apt install postgresql-client```
- formatage et requêtage `json` : ```sudo apt install jq```

Démarrage des services :

- Docker postgres => ```vm-postgres/run.sh```
- Docker VM de test => ```vm-ubuntu/run.sh``` ==> ```ssh node@IP```

On va utiliser la VM ubuntu pour la suite des évènements.



## Rotation d'un mot de passe de base de données

~~~bash
# Check de l'acces à postgres (root/rootpassword)
$> psql --host postgres --user root --port 5432 postgres
psql (10.10 (Ubuntu 10.10-0ubuntu0.18.04.1), server 12.1 (Debian 12.1-1.pgdg100+1))
WARNING: psql major version 10, server major version 12.
         Some psql features might not work.
Type "help" for help.

root=# \du
                                   List of roles
 Role name |                         Attributes                         | Member of 
-----------+------------------------------------------------------------+-----------
 root      | Superuser, Create role, Create DB, Replication, Bypass RLS | {}

# Connexion en tant qu'administrateur
$> vault login -method=userpass username=admin password=admin
Success! You are now authenticated. The token information displayed below
is already stored in the token helper. You do NOT need to run "vault login"
again. Future Vault requests will automatically use this token.

Key                    Value
---                    -----
token                  s.nmBabz3qMNesYCQyujdRWjsn
token_accessor         HezkqMUMDH84QOdBcjKo9yTu
token_duration         768h
token_renewable        true
token_policies         ["default"]
identity_policies      ["admin"]
policies               ["admin" "default"]
token_meta_username    admin

# Ajout du moteur de secret 'database'
$> vault secrets enable database
Success! Enabled the database secrets engine at: database/


# Nous allons dans un premier temps nous occuper du mot de passe root de la base, en établissant une rotation du mot de passe.

# On se connecte en tant que provisionner
$> vault login -method=userpass username=provision password=provision
Success! You are now authenticated. The token information displayed below
is already stored in the token helper. You do NOT need to run "vault login"
again. Future Vault requests will automatically use this token.

Key                    Value
---                    -----
token                  s.R3BtjuE7K8d2b1ssguqvAy7E
token_accessor         np3c3UcJPYUgwh34ZeyGhybF
token_duration         768h
token_renewable        true
token_policies         ["default"]
identity_policies      ["provisioner"]
policies               ["default" "provisioner"]
token_meta_username    provision

# 
# Configuration de l'acces à la base de données pour Vault
$> vault write database/config/postgresql \
	plugin_name=postgresql-database-plugin \
	connection_url="postgresql://{{username}}:{{password}}@postgres:5432/postgres?sslmode=disable" \
	allowed_roles="*" \
	username="root" \
	password="rootpassword"
	
# On force la rotation du mot de passe
$> vault write -force database/rotate-root/postgresql

# Test, le mot de passe 'rootpassword' ne fonctionne plus
$> psql --host postgres --user root --port 5432 postgres
Password for user root: 
psql: FATAL:  password authentication failed for user "root"

##
## Warning : une fois que l'on a mis en place la rotation, seul Vault connait le password
## Il faudra peut-etre reserver un user admin a cet usage.
##
~~~



## configuration d'un accès en lecture à la base.

~~~bash
# Create readonly.sql defining the role privilege
$> tee readonly.sql <<EOF
CREATE ROLE "{{name}}" WITH LOGIN PASSWORD '{{password}}' VALID UNTIL '{{expiration}}';
GRANT SELECT ON ALL TABLES IN SCHEMA public TO "{{name}}";
EOF

# Create a role named 'readonly' with TTL of 1 hr
$> vault write database/roles/readonly db_name=postgresql \
         creation_statements=@readonly.sql \
         default_ttl=1h max_ttl=24h
Success! Data written to: database/roles/readonly

# Get a new set of credentials
$> vault read database/creds/readonly
Key                Value
---                -----
lease_id           database/creds/readonly/9PEfC14ipekpnqSOX00S82bZ
lease_duration     1h
lease_renewable    true
password           A1a-zS958ahuYHxlvEah
username           v-userpass-readonly-2qqTEgfcpD5eooiSYZOx-1578384415


# Make sure that you can connect to the database using the Vault generated credentials
$> psql --host postgres --port 5432 \
        --user v-userpass-readonly-2qqTEgfcpD5eooiSYZOx-1578384415 postgres
Password for user v-userpass-readonly-2qqTEgfcpD5eooiSYZOx-1578384415: 
psql (10.10 (Ubuntu 10.10-0ubuntu0.18.04.1), server 12.1 (Debian 12.1-1.pgdg100+1))
WARNING: psql major version 10, server major version 12.
         Some psql features might not work.
Type "help" for help.

postgres=> \du
                                                        List of roles
                      Role name                      |                         Attributes                         | Member of 
-----------------------------------------------------+------------------------------------------------------------+-----------
 root                                                | Superuser, Create role, Create DB, Replication, Bypass RLS | {}
 v-userpass-readonly-2qqTEgfcpD5eooiSYZOx-1578384415 | Password valid until 2020-01-07 09:07:00+00                | {}

postgres=> \q
~~~



#### Creation de droits d'acces à une base pour mon application.

~~~bash
#Mise en place de policy pour mon application
$> tee apps-policy.hcl << EOL
# Get credentials from the database secrets engine
path "database/creds/readonly" {
  capabilities = [ "read" ]
}
EOL

# Creation de la policy 'apps'
$> vault policy write apps apps-policy.hcl
Success! Uploaded policy: apps


# Creation du token pour l'application
$> vault token create -policy="apps"
Key                  Value
---                  -----
token                s.fqscTFMmgJhFskkLrb7DxEZK
token_accessor       SzVHuVSdtzcCmwtcRekrd3yd
token_duration       768h
token_renewable      true
token_policies       ["apps" "default"]
identity_policies    ["admin"]
policies             ["admin" "apps" "default"]


##
## Warning : Cette methode fournit un token qui additione les droits. A l'usage nous aurons les droits admin et apps.
##

# Une méthode un peu plus propre
$> vault token create -policy="apps" -orphan
Key                  Value
---                  -----
token                s.dgSyuUOZPIDdTLMWoWMkqazS
token_accessor       f82KPW5mCPih3uNl2CSP0w1J
token_duration       768h
token_renewable      true
token_policies       ["apps" "default"]
identity_policies    []
policies             ["apps" "default"]

# Obtention d'un nouveau jeu d'acces à la base, avec mes droits d'application
$> VAULT_TOKEN=s.dgSyuUOZPIDdTLMWoWMkqazS vault read database/creds/readonly
Key                Value
---                -----
lease_id           database/creds/readonly/oyIo0P9KrOyDdtrI4YyYCJsm
lease_duration     1h
lease_renewable    true
password           A1a-kwIGwS23XhZpfGEo
username           v-token-readonly-2Sp0p3HPgCdxoWHc21oy-1578386964

# Test de l'acces à la base
psql --host postgres --port 5432 \
>         --user v-token-readonly-2Sp0p3HPgCdxoWHc21oy-1578386964 postgres
Password for user v-token-readonly-2Sp0p3HPgCdxoWHc21oy-1578386964: 
psql (10.10 (Ubuntu 10.10-0ubuntu0.18.04.1), server 12.1 (Debian 12.1-1.pgdg100+1))
WARNING: psql major version 10, server major version 12.
         Some psql features might not work.
Type "help" for help.

postgres=> \du
                                                        List of roles
                      Role name                      |                         Attributes                         | Member of 
-----------------------------------------------------+------------------------------------------------------------+-----------
 root                                                | Superuser, Create role, Create DB, Replication, Bypass RLS | {}
 v-token-readonly-2Sp0p3HPgCdxoWHc21oy-1578386964    | Password valid until 2020-01-07 09:49:29+00                | {}
 v-userpass-readonly-2qqTEgfcpD5eooiSYZOx-1578384415 | Password valid until 2020-01-07 09:07:00+00                | {}

postgres=> \q

~~~



Comme méthode c'est plutot propre, rapidement j'imagine

1. Reservation du compte d'administration principal de la base dans un coffre fort physique. (solution de Disaster Recovery)
2. Creation d'un compte d'administration géré exclusivement par Vault, avec rotation régulière.
3. Un role par besoin d'acces à une base.