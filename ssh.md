# SSH

Doc :

- https://www.vaultproject.io/docs/secrets/ssh/index.html
- https://www.vaultproject.io/docs/secrets/ssh/signed-ssh-certificates.html
- https://www.vaultproject.io/docs/secrets/ssh/one-time-ssh-passwords.html



Un article intéressant : https://blog.octo.com/gardez-les-cles-de-votre-infrastructure-a-labri-avec-vault/



2 modes :

- [certificat](#ssh-via-certificat-signé)
- [One Time Password](#ssh-via-one-time-password)



#### SSH via Certificat signé





~~~bash
$> vault login -method=userpass username=provision password=provision
Success! You are now authenticated. The token information displayed below
is already stored in the token helper. You do NOT need to run "vault login"
again. Future Vault requests will automatically use this token.

Key                    Value
---                    -----
token                  s.u8RUcUMio7otQTx1EmspylEW
token_accessor         1rdoEtvrCHvaS70c45Bs5daU
token_duration         768h
token_renewable        true
token_policies         ["default"]
identity_policies      ["provisioner"]
policies               ["default" "provisioner"]
token_meta_username    provision

# Ajout du moteur ssh
$> vault secrets enable -path=ssh-client-signer ssh
Success! Enabled the ssh secrets engine at: ssh-client-signer/

# Generation d'une clef de CA pour Vault
$> vault write ssh-client-signer/config/ca generate_signing_key=true
Key           Value
---           -----
public_key    ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDUYLmBQtGNdLBXH1P2ZsER9U1SbxYofBJk/JmTRqfr4k4FRBWA+cAvmnzcg4DsWgroyMU+XJ1ABN67SZOC6idfE6xzcw52CZMckwjY8q4Lovl8Kv88v6A4t9sxCvQnjNW7+7QRmHVO2nnujShH4HiaFs6PN1IYVMmK6S4xjCQul3mNV9dXTxjpiT32Ba0Pcid5v5CSVbq3FkWefi4zPA3kOx26Bls2SzXOMydKmFVUSCREz19zrWr9clMG5XTTTc/xkyw3o4Ul9Gr9uFx0xHiUf7Tc56t2FuXcMo7yxVc9J4omSSN+fCGYl6KtGoJ5rh5fGTq38kLAEQabKp7mjlP7GwlGGRCemmkbG6DJSSweZ/I+sBPXoHpFMo+LO+BVB1miTZTJTIhY1aTJK3LhKXvt6B9cBrtLi+MY7VbhfRhREe1ZtaHGajDjye0VjvxMxS8/+HKy7BLc3JOcmLt+mZsr07nn+Gs9oRPUs/RCbOXJhpGtb+fEkVtIMZrK5+Z+oc5sGwKj2oXBW70TEaILcA8KvnbKj4WzMrjIOK+UmbG/hd0w9wN+SJ+aIdkwAw4lllCwUnDt4Ro4lq0Zg8de3QDn9QJdeUAAW22ulDBcu9dB90QCMp72QBl/g57ZM3N8ejDgn1Yk5Q7va8c5K19vE/qSIy01kM9/W8ka2qBeJ0+isw==

# Demarrage de la VM test Docker ssh-signer (Depuis la machine hote)
$> vm-ssh/run.sh 
...
.
=====================================================
VM : 
     - name : ssh-signer ('ssh-signer.vaultnet')
     - IP : 172.18.0.4
     - SSH : ssh -o StrictHostKeyChecking=no node@172.18.0.4
=====================================================

# Test (Reprise des commandes depuis la VM de test)
$> ssh -o StrictHostKeyChecking=no node@172.18.0.4 hostname
node@172.18.0.4's password: 
ssh-signer.vaultnet


# Generation d''un role d''acces
$> vault write ssh-client-signer/roles/my-role -<<"EOH"
{
  "allow_user_certificates": true,
  "allowed_users": "*",
  "default_extensions": [
    {
      "permit-pty": ""
    }
  ],
  "key_type": "ca",
  "default_user": "node",
  "ttl": "1m0s"
}
EOH

# Generation de notre clef SSH
$> ssh-keygen -t rsa -N '' -f $HOME/.ssh/id_rsa -C 'My SSH Key'
Generating public/private rsa key pair.
Created directory '/home/node/.ssh'.
Your identification has been saved in /home/node/.ssh/id_rsa.
Your public key has been saved in /home/node/.ssh/id_rsa.pub.
The key fingerprint is:
SHA256:BKmVh4OBo8uIGy3ik+3XV/ahR4dRk6Y9waEnzgnPsgk My SSH Key
The key's randomart image is:
+---[RSA 2048]----+
|   ..o.+      ..o|
|  o . *..     .B |
| . . o o.  . o=.o|
|.   .  .    *o+o |
|+o      SE . *o .|
|B..       .o++ . |
|o+o   .   oo+ o  |
|.+ . . . . . o   |
|  o..   .   .    |
+----[SHA256]-----+

$> vault write -field=signed_key ssh-client-signer/sign/my-role \
     public_key=@$HOME/.ssh/id_rsa.pub > signed-cert.pub
ssh-rsa-cert-v01@openssh.com AAAAHHNzaC1yc2EtY2VydC12MDFAb3BlbnNzaC5jb20AAAAgBQO+g0n0NHRfSs9uAie+dHFBl2tPgEfqZhw8pNkr8+IAAAADAQABAAABAQDi+UG8E1vb4Yf4JpiQ1zOvMf9DYjpNIs6gMqFC4Zt2QlD5J1fESZUyIRTLMMlaPYtaYti3Tzdn0YHYHm7K8f0apaNzoci3HTxFr1o64dITBYyW7kOEgVZ1tz4GrLlSTdr56OFKY92LOgtM7Hc3jJsRZvMljq7dk/PKoTJ7KrtoSKODGaxqKRnksXAWI6zFshKUpoWb/ZUvu0bdtQQvRjRElDZXeORgnNUQTO6vGM0sdiGXuF5a4G8T4ppd3+2ZPVJC5c/ujLJVk1zW4AcBhAg0eXQsNhHdOaOkMiqGb51fgbnKJSuz+XKGfDeMiemz4EgAozOjYIvqLxXHk+vJ+3W/KaElw72bibcAAAABAAAAVXZhdWx0LXVzZXJwYXNzLWFkbWluLWY1NTkxYTAwMjIwYjZkMTI4YWRiOTU2NTdkMWMxNzZmNGFmOTIyZjgzZGQ1OTcyYmI3MjMyMWE0N2EzNTA3MDAAAAAIAAAABG5vZGUAAAAAXhiCqAAAAABeGIMCAAAAAAAAABIAAAAKcGVybWl0LXB0eQAAAAAAAAAAAAACFwAAAAdzc2gtcnNhAAAAAwEAAQAAAgEAnbjKKoC/SfTm/ICWbEcOyc97HU0fQjpA9DtP0kPdSB8yv1dCNUZT7iz83eoJKnWIMMsxdZLR/HudTnX8X8SziAuIOtvQVvY0hu3k2OGu2stYw1CPdaBI5OxLEg1JXrQxzIzqaYJ152VMNssP5LMTq5+2RTvsK/e5TG1vWUwHLINiEP5qbr8usRCJfyyL6zsTyp5sUszbkWoHw3ulm7fKsBvh7KLFz6lQIV4CQsGGLVRP7+/Ap5/3JfHChC0yKxdysAREyvRh/9m5G7m0yLWH3Q/QSEjoaKXkSlfqcTNnsHpnlrHhRIYpDZ3XMLxCR+dzseZHetDhFrkzYJNWDlBI74BZJqSo5vHjdDACqwPSQIt5gBb+7Cstuh0uTb1iP5cUccRnOrdBBU1LxR0z028mlhHRKVRtu/ETdK/Gi+sOepjymzY7x9PTIKWb/qD0iJ2P1Dix7RPnKllzzfuh758FhQEXi3HTb2IeIaHote+ynFSqKM/OMbx82PmqX8XFWWTexY3vIzIX+gg3YdUOHEMW0TkkrLcCfQXjddK1eisCKZ91ye143Td3IZt4ooflTqOeX2ij24vEF+ch+qPHmQrM8GNmLEWiwqwk05/NdLrbjKNqmq1BwYk1oYxR5LBzzMED88p1apvzMRlX7f05E1TngP8lAzA8hjaOlb+Xw1RdVksAAAIPAAAAB3NzaC1yc2EAAAIAZMn78gRSlSGh02M9YTIx+TaSHx9uhm8+Im+659VFHFP4ZCNp1WWtLwKVE70ezWkY5MDUIWuQ867dTLdRcRleThzTtmL1Z9sDAVUjKsNds+wiRdC1ApqXjw7Q3XqUsrk0AoCx6oVlesM+AT+8x4XMm5UQFkk4MWS972D9tpLBGQpioZUV7jZeLXcE+OztPqfBWZqstHKGoJqNgKstaGoTQBklhiKTGX+96jm6JbZiX2w+mtOhf2VmTe816f3OJszIoffsoDw1sa0fYrFajZsBDVXeQBCfHviPAb5B0M8+kC5oUFR2fHkz4rZ3JXHoB9R1E5X/wmidCY/5SfYV3CyEKXsL5ysULPn+pKymyqe3wwunq0klLPC529EEJZXgMsHvWlqcLPw2g1VKnsJEJUT7bDyOzycFZJsDYL4OpNyVUBtVXGvBTL1OYSVmX1hR2+CGsR0QVTL23sv3VCxyJOYg6LtIHWWzKScrJL0Ih8Qk1JKdcbI+AlsdZM2b01SB8G9/cSwm/u5H7slGevl2aEafxqkeBDENr8jdxDCew9xsGeEKmiIgYR4p76zQDdRJ1yPONGL+9ek7EpKGwPEcxZv2ZqKgEWm/wu/3TMcHu/iJ1kFnp9loKlwccmf9uibQ+PvKFE3tEvEP+d5GbpR1ivUK4iqNoKivhs8gcX/fG4QCNog=

# Test de connexion
$> ssh -i signed-cert.pub -i ~/.ssh/id_rsa -o StrictHostKeyChecking=no node@172.18.0.4 hostname
ssh-signer.vaultnet

# Contenu de la clef signée
$> ssh-keygen -Lf ./signed-cert.pub
./signed-cert.pub:
        Type: ssh-rsa-cert-v01@openssh.com user certificate
        Public key: RSA-CERT SHA256:9VkaACILbRKK25VlfRwXb0r5Ivg91ZcrtyMhpHo1BwA
        Signing CA: RSA SHA256:Amb7VqimqE+sdWC74sue2ttAIuVPzoLQj9wcVJ/YhhM
        Key ID: "vault-userpass-admin-f5591a00220b6d128adb95657d1c176f4af922f83dd5972bb72321a47a350700"
        Serial: 15429559505687267269
        Valid: from 2020-01-10T13:59:27 to 2020-01-10T14:00:57
        Principals: 
                node
        Critical Options: (none)
        Extensions: 
                permit-pty

# Test une minute apres
$> ssh -i signed-cert.pub -i ~/.ssh/id_rsa -o StrictHostKeyChecking=no node@172.18.0.4 hostname
node@172.18.0.4's password:


~~~



**Warning** : Solution très élégante, mais avec une énorme limitation, impossible de donner des droits d'accès par serveur. Nous avons bien la possibilité de ré-instancier plusieurs fois le moteur SSH (ainsi que les namespaces), mais ca reste du bricolage pour donner des droits fins. Le certificat signé est valable sur tous les serveurs qui accepte l'AC installé, via nos droits vault, il est possible d'en récupérer un pour un serveur légitime (via nos droits vault), et de l'utiliser sur un autre serveur (tant que le délai d'usage ne sera pas dépassé).



#### SSH via One-Time Password

Nécessite l'emploie de [Hashicorp Vault SSH Helper](https://github.com/hashicorp/vault-ssh-helper) qui est module PAM qui va substituer la validation par mot de passe standard par un check dans Vault, ainsi qu'une connexion permanente entre les serveurs SSH et Vault.

....



Solution beaucoup moins élégante que la précédente, mais qui répond au besoin de check par serveur et par user. En démultipliant le nombre de roles par serveur/user.