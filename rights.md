# User and rights



prérequis :

- formatage et requêtage json : ```sudo apt install jq```



Démarrage des services :

- Docker Vault => ```vm-vault/run.sh```
- Docker VM de test => ```vm-ubuntu/run.sh``` ==> ```ssh node@IP```

Script final : 

- sur la VM de test [dev/vault/init-user.sh](vm-ubuntu/resources/vault/init-user.sh)



Jusqu'à présent nous avons tout fait avec le user root. Nous allons maintenant regarder comment créer des users avec des privilèges restreint.

- un admin : qui aura le rôle de créer/administrer les différents moteurs de Vault.
- un provisioner : qui aura le rôle d'alimenter les moteurs en secrets et de les gérer dans le temps.
- Nous verrons plus tard pour les users applicatifs.



### Check des prérequis

Sur la VM de test, vous avez :

- url du server vault déjà pré-renseigné : ```export VAULT_ADDR='http://host.docker.internal:8200'```
- Vault présent dans le path : ```vault status``` doit répondre qu'il arrive bien à pinger le service
- Se connecter en root, via le token Root indiqué au démarrage de Vault : ```vault login ....```



 ### Policies

- Création des politiques de sécurité (Policy) : https://learn.hashicorp.com/vault/identity-access-management/iam-policies

Avec ce tuto, Nous apprenons à créer les policy [admin](vm-ubuntu/resources/vault/admin-policy.hcl) et [provisioner](vm-ubuntu/resources/vault/provisioner-policy.hcl).

~~~bash
# Creation du policy admin
$> vault policy write admin admin-policy.hcl
Success! Uploaded policy: admin

# Creation du policy provisionner
$> vault policy write provisioner provisioner-policy.hcl
Success! Uploaded policy: provisioner

# List des policies
$> vault policy list
admin
default
provisioner
root

~~~



  Pour aller plus loin, Vault propose aussi des [notions de namespaces](https://learn.hashicorp.com/vault/operations/namespaces).



### Users

Cf : https://learn.hashicorp.com/vault/identity-access-management/iam-identity

Ce tuto montre comment créer des entity et des groupes. A terme, ces éléments seront dans un ldap et ce ne sera pas le problème de vault de gérer les groupes et ses users, juste de mapper des groupes annuaire à des policies.

Sur le principe :

- une identité représente identité de la personne physique et dispose de plusieurs moyens de se connecter (users).
- un User est la représentation technique de connexion (c'est lui qui est relié aux différents moteurs de connexion => ldap, gitlab, etc..).
- une identité peut être associé à plusieurs groupes
- un groupe peut être associé à plusieurs policies.

*Warning* : La doc sur vault montre que chacun des éléments (entity, user, groupe) sont optionnel, et chaque éléments peut-être associé à des policies.

~~~bash
# Creation de l'entité Administrator
$> vault write identity/entity name="administrator" \
        metadata=organization="ACME Inc."
Key        Value
---        -----
aliases    <nil>
id         f12e2079-df89-4aaf-9168-78c7b2f0ce6f
name       administrator

##
## Pour la suite, il faut sauvegarder cet id dans un fichier 'entity_id.txt'
##

# Creation du groupe admin-group relie à la policy admin
$> vault write identity/group name="admin-group" \
        policies="admin" \
        member_entity_ids="f12e2079-df89-4aaf-9168-78c7b2f0ce6f"  \
        metadata=team="Vault Administors"
Key     Value
---     -----
id      0a4b3839-4693-eb8b-4d38-f7f80a96cf34
name    admin-group

# Ajout du moteur userpass aux mécanismes d'identification
$> vault auth enable userpass
Success! Enabled userpass auth method at: userpass/

# Creation du user admin
$> vault write auth/userpass/users/admin password="admin"
Success! Data written to: auth/userpass/users/admin

# Association du user admin à l'entité administrator
$> vault write identity/entity-alias name="admin" \
                canonical_id="$(vault read -format=json identity/entity/name/administrator | jq '.data.id' | tr -d '"')" \
                mount_accessor="$(vault auth list -format=json | jq -r '.["userpass/"].accessor')"
Key             Value
---             -----
canonical_id    f12e2079-df89-4aaf-9168-78c7b2f0ce6f
id              2b144af3-40c3-e9a1-2ae5-be8b8f06adb7

## Test
$> vault login -method=userpass username=admin password=admin
Success! You are now authenticated. The token information displayed below
is already stored in the token helper. You do NOT need to run "vault login"
again. Future Vault requests will automatically use this token.

Key                    Value
---                    -----
token                  s.NF231G5Zw7iENUaDtRMnxf1m
token_accessor         7aoVYJdBNLGvvIc6M7GJS7Pv
token_duration         768h
token_renewable        true
token_policies         ["default"]
identity_policies      ["admin"]
policies               ["admin" "default"]
token_meta_username    admin

$> vault secrets list
Error listing secrets engines: Error making API request.

URL: GET http://localhost:8200/v1/sys/mounts
Code: 403. Errors:

* 1 error occurred:
	* permission denied
# Erreur normal, nous ne sommes plus connecte en tant que root.
~~~



Faire la même chose pour le user ***provisioner***

- entity : **provisioner**
- group : **provision-group** avec policy : *provisioner*
- user : **provision** / **provision**