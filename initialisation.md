# Initialisation VM Vault

Dans cette exemple, on lance Vault dans un container docker, par la suite nous aurons besoin de lancer plusieurs application. L'objectif étant de ne pas pourrir son installation locale avec des outils/user inutile au delà de ce test.

Cf : https://hub.docker.com/_/vault

script final : [vm-vault/run.sh](vm-vault/run.sh)

~~~bash
## Creation d'un reseau docker pour les exercices
$> docker network create --driver bridge vaultnet

# Demarrage de la VM Vault
# Pour l'exemple on configure Vault sur le port 1234
$> docker run --cap-add=IPC_LOCK -e 'VAULT_DEV_LISTEN_ADDRESS=0.0.0.0:1234' -p 8200:1234 -d --name=dev-vault.example.net --rm -h dev-vault.example.net --network vaultnet vault

# Configuration du vault sur le host local
# Pour les vault sur les autres VM docker ce sera : http://dev-vault.example.net:1234
$> export VAULT_ADDR='http://127.0.0.1:8200'

# Recupération des Token (unseal et root)
$> docker logs dev-vault 2>&1 | egrep '^Root Token:|^Unseal Key:'
Unseal Key: kGWug5//xQwTkGKIAonBjvauKi1qtxhmOwdKwJLAIcA=
Root Token: s.qQeabzexkqpthCQPGJj18Dae

# Test de l'acces à Vault
$> vault status
Key             Value
---             -----
Seal Type       shamir
Initialized     true
Sealed          false
Total Shares    1
Threshold       1
Version         1.3.1
Cluster Name    vault-cluster-9b7c0c88
Cluster ID      52343c24-e9f8-d352-23a5-9d80b20e816d
HA Enabled      false

# Test des droits
# 1er essai KO, normal nous n'avons pas les droits de voir le contenu du vault
# Soit nous n'avons pas Token Vault, soit ce n'est pas le bon
$> vault secrets list
Error listing secrets engines: Error making API request.

URL: GET http://127.0.0.1:8200/v1/sys/mounts
Code: 400. Errors:

* missing client token

# Connexion en Root
$> vault login s.qQeabzexkqpthCQPGJj18Dae
Success! You are now authenticated. The token information displayed below
is already stored in the token helper. You do NOT need to run "vault login"
again. Future Vault requests will automatically use this token.

Key                  Value
---                  -----
token                s.qQeabzexkqpthCQPGJj18Dae
token_accessor       aGTrY0PFwcwchjTKNFTWNL01
token_duration       ∞
token_renewable      false
token_policies       ["root"]
identity_policies    []
policies             ["root"]

# Test d'acces
$> vault secrets list
Path          Type         Accessor              Description
----          ----         --------              -----------
cubbyhole/    cubbyhole    cubbyhole_564f0002    per-token private secret storage
identity/     identity     identity_c282d754     identity store
secret/       kv           kv_dc3fc805           key/value secret storage
sys/          system       system_74b05929       system endpoints used for control, policy and debugging

~~~

