#!/bin/bash
# Creation du network vaultnet
if [ $(docker network ls | grep 'vaultnet' > /dev/null 2>&1; echo $?) -ne 0 ]; then
	echo "Creation du docker network 'vaultnet'."
	docker network create --driver bridge vaultnet
fi

cd $(pwd)/$(dirname $0)
docker run --name postgres -e POSTGRES_USER=root -e POSTGRES_PASSWORD=rootpassword -h postgres.example.net --dns-search=example.net --network vaultnet -d -p 5432:5432 --rm postgres

