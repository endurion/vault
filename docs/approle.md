# Gestion des AppRole

AppRole sert à authentifier les applications qui n'ont pas de moteur naturel permettant de l'identifier (comme Kubernetes). Globalement cela ressemble au moteur d'authentification 'userpass' utilisé précédemment mais orienté application.



Doc :
- [AppRole Auth Method](https://www.vaultproject.io/docs/auth/approle/)
- [Tuto AppRole Pull Authentication](https://learn.hashicorp.com/vault/identity-access-management/iam-authentication)



![AppRole auth method workflow](./images/vault-approle-workflow.png)

## Initialisation des droits

Nous allons rajouter ces droits au rôle [admin](vm-ubuntu/resources/vault/admin-policy.hcl).

~~~bash
# Mount the AppRole auth method
path "sys/auth/approle" {
  capabilities = [ "create", "read", "update", "delete", "sudo" ]
}

# Configure the AppRole auth method
path "sys/auth/approle/*" {
  capabilities = [ "create", "read", "update", "delete" ]
}

# Create and manage roles
path "auth/approle/*" {
  capabilities = [ "create", "read", "update", "delete", "list" ]
}

# Write ACL policies
path "sys/policies/acl/*" {
  capabilities = [ "create", "read", "update", "delete", "list" ]
}
~~~



Pour l'initalisation des droits, voir la section [User and rights](./rights.md).

~~~bash
# Connexion en tant qu'admin
$> vault login -method=userpass username=admin password=admin
~~~



## Installation de l'authentification via l'AppRole Auth Method

~~~bash
# installation de l'authent AppRole
$> vault auth enable approle
Success! Enabled approle auth method at: approle/
~~~



## Création d'un role Application avec une policy

Pour le besoin du POC nous allons simuler l'accés à une données dans le moteur KV : `/secret`.

Nous rajoutons ces droits dans le fichiers [apps-policy.hcl](vm-ubuntu/resources/vault/apps-policy.hcl)

~~~bash
# Read-only permission on 'secret/data/mysql/*' path
path "secret/data/mysql/*" {
  capabilities = [ "read" ]
}
~~~

~~~bash
# Creation de la policy
$> vault policy write apps apps-policy.hcl

# Creation du Role pour mon application apps
$> vault write auth/approle/role/apps token_policies="apps" \
        token_ttl=1h token_max_ttl=4h
Success! Data written to: auth/approle/role/apps

# Check du role apps
$> vault read auth/approle/role/apps
Key                        Value
---                        -----
bind_secret_id             true
local_secret_ids           false
secret_id_bound_cidrs      <nil>
secret_id_num_uses         0
secret_id_ttl              0s
token_bound_cidrs          []
token_explicit_max_ttl     0s
token_max_ttl              4h
token_no_default_policy    false
token_num_uses             0
token_period               0s
token_policies             [apps]
token_ttl                  1h
token_type                 default

~~~



## Obtention des credentials pour application

~~~bash
# Get RoleID
$> vault read auth/approle/role/apps/role-id
Key        Value
---        -----
role_id    7a89fc47-6fe7-2553-aa55-bdbfd3528ecc

# Generation du secretID
$> vault write -force auth/approle/role/apps/secret-id
Key                   Value
---                   -----
secret_id             b0d54bb8-d474-692c-d821-e0a41d2f0b7e
secret_id_accessor    1f3754d3-401b-7bd9-5a85-8f1ff35a71ee

~~~



## Login avec RoleID && SecretID

~~~bash
$> vault write auth/approle/login role_id="7a89fc47-6fe7-2553-aa55-bdbfd3528ecc" \
  secret_id="b0d54bb8-d474-692c-d821-e0a41d2f0b7e"
Key                     Value
---                     -----
token                   s.Ue40MWUfyOsz96LkeHzP5EBi
token_accessor          S52jBOyGvbTWjgmTCx7UNSxc
token_duration          1h
token_renewable         true
token_policies          ["apps" "default"]
identity_policies       []
policies                ["apps" "default"]
token_meta_role_name    apps

~~~



Nous récupérons ainsi un token Vault, qui va nous permettre d'interagir avec Vault.

Bien sur, la creation du token AppRole est accessible par n'importe qui, sans authentification Vault préalable.

~~~bash
# Exemple acces sans authentification
$> VAULT_TOKEN=blabla vault write auth/approle/login \
  role_id="7a89fc47-6fe7-2553-aa55-bdbfd3528ecc" \
  secret_id="b0d54bb8-d474-692c-d821-e0a41d2f0b7e"
Key                     Value
---                     -----
token                   s.cH7az6DUgH34Q2gpEhlwxATj
token_accessor          ei4fnSRosFa9zY0RVRBqQ5n7
token_duration          1h
token_renewable         true
token_policies          ["apps" "default"]
identity_policies       []
policies                ["apps" "default"]
token_meta_role_name    apps

~~~



## Lecture d'un secret via Token AppRole

~~~bash
$> VAULT_TOKEN=s.Ue40MWUfyOsz96LkeHzP5EBi vault kv get secret/mysql/webapp
No value found at secret/mysql/webapp

# Test creation d'un secret
$> tee mysqldb.txt <<"EOF"
{
  "url": "foo.example.com:35533",
  "db_name": "users",
  "username": "admin",
  "password": "pa$$w0rd"
}
EOF

$> vault login -method=userpass username=admin password=admin
...
$> vault kv put secret/mysql/webapp @mysqldb.txt
Key              Value
---              -----
created_time     2020-02-11T08:47:29.93838671Z
deletion_time    n/a
destroyed        false
version          1

# Test de lecture via Token AppRole
$> VAULT_TOKEN=s.Ue40MWUfyOsz96LkeHzP5EBi vault kv get secret/mysql/webapp
====== Metadata ======
Key              Value
---              -----
created_time     2020-02-11T08:47:29.93838671Z
deletion_time    n/a
destroyed        false
version          1

====== Data ======
Key         Value
---         -----
db_name     users
password    pa$$w0rd
url         foo.example.com:35533
username    admin

~~~



