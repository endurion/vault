# Moteur de secret Cubbyhole

Ce moteur de secret sert de conteneur temporaire de secret, pour permettre un échange entre 2 parties du Vault, sans droit particulier sur les secrets d'origine.



Docs :

- [CubbyHole Secret Engine](https://www.vaultproject.io/docs/secrets/cubbyhole/index.html)
- [Learn Cubbyhole](https://learn.hashicorp.com/vault/secrets-management/sm-cubbyhole)



![Response Wrapping Scenario](./images/vault-cubbyhole.png)