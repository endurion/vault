# Vault

L'objectif de ce projet est de s'approprier Hashicorp Vault à travers quelques exemple et la documentation de l'outils.



Les répertoires de ce projet représentent les quelques VM Doc :

- vm-vault : Un docker pour le lancement d'un Vault en mode développement et sans conservation des données.
- vm-ubuntu : le serveur de développement conçu pour lancer les différents tests.
- vm-postgres : une instance postgres pour les tests d'automatisation des accès bases de données



## Formation :

- Généralités :
  - Download : https://www.vaultproject.io/downloads.html
  - Installation : https://learn.hashicorp.com/vault/getting-started/install
  - Lancement serveur : https://learn.hashicorp.com/vault/getting-started/dev-server
  - 1er pas : https://learn.hashicorp.com/vault/getting-started/first-secret

- [initialisation docker Vault](./initialisation.md)

- [Gestion des utilisateurs et des droits](./rights.md)

  - [Auth AppRole](./docs/approle.md)

- Moteur de secrets dynamique

  - [Databases](./database.md)
  - [SSH](./ssh.md)
  - [PKI](./pki.md)
  - [cubbyhole](./docs/cubbyhole.md)

  
